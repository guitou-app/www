import React from 'react';
import { Button, Space } from 'antd';

export default function Header(props) {
  return (
    <header {...props}>
      <a className="logo-wrapper" href="http://www.guitou.cm" target="_blank">
        <i className="logo" />
        <span>Guitou</span>
      </a>
      <div className="button">
        <Space>
          <a target="_blank" href={`${process.env.REACT_APP_APP_URL}/auth/sign-up`} key="up">
            <Button type="primary">Sign Up</Button>
          </a>
          <a target="_blank" href={`${process.env.REACT_APP_APP_URL}/auth/sign-in`} key="in">
            <Button>Sign In</Button>
          </a>
        </Space>
      </div>
    </header>
  );
}
