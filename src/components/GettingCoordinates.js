import React, { useState } from 'react';
import { 
  Result,
  Spin,
  Typography,
} from 'antd';
import {
  CloseCircleOutlined
} from '@ant-design/icons';
import { geolocated } from "react-geolocated";
import Xorm from "./Xorm";

const { Paragraph, Text } = Typography;

const GeolocationUnavailable = () => (
  <main>
    <Result
      status="error"
      title="Geolocation not supported"
      subTitle="Your browser does not support Geolocation. Geolocation is needed for filling this form."
    />
  </main>
);

const GeolocationDisabled = () => (
  <main>
    <Result
      status="error"
      title="Geolocation not enabled. You can not fill up the survey."
    >
      <div className="desc">
        <Paragraph>
          <Text
            strong
            style={{
              fontSize: 16,
            }}
          >
            Geolocation is not enabled. Kindly enabled it up by clicking at the left of the URL up there.
            You can follow one of this link according to your browser.
          </Text>
        </Paragraph>
        <Paragraph>
          <CloseCircleOutlined className="site-result-demo-error-icon" />
          {' '}
          Google
          {' '}
          <a target="_blank" href="https://support.google.com/chrome/answer/142065?co=GENIE.Platform%3DDesktop&hl=en">click here</a>
        </Paragraph>
        <Paragraph>
          <CloseCircleOutlined className="site-result-demo-error-icon" />
          {' '}
          Firefox
          {' '}
          <a target="_blank" href="https://support.mozilla.org/en-US/kb/does-firefox-share-my-location-websites">click here</a>
        </Paragraph>
      </div>
    </Result>
  </main>
);

const Geolocation = (props) => {
  const {
    isGeolocationAvailable, isGeolocationEnabled, coords,
    positionError, positionOptions, onSuccess,
  } = props;

  const renderCoordinates = () => {
    return coords ? (
      <Xorm coords={coords} />
    ) : (
      <h1>Getting coordinates</h1>
    );
  };

  const renderGeolocationAvailable = () => {
    return isGeolocationEnabled ? (
      renderCoordinates()
    ) : (
      <GeolocationDisabled />
    );
  };

  return isGeolocationAvailable ? (
    renderGeolocationAvailable()
  ) : (
    <GeolocationUnavailable />
  );
};

const GeolocatedXorm = geolocated()(Geolocation);

const GettingCoordinates = () => {
  const [isGettingCoordinates, setGettingCoordinates] = useState(true);

  return (
    <Spin size="large" tip="is getting coordinates" spinning={isGettingCoordinates}>
      <GeolocatedXorm
        onSuccess={position => setGettingCoordinates(!isGettingCoordinates)}
        onError={position => setGettingCoordinates(!isGettingCoordinates)}
      />
    </Spin>
  );
};

export default GettingCoordinates;
