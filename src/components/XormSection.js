import React, { useEffect, useContext, useState, useMemo, memo } from 'react';
import {
  Form,
  Input,
  InputNumber,
  Radio,
  Checkbox,
  Select,
  Row, Col,
  DatePicker, TimePicker,
  Rate,
  Slider,
  Typography,
  Divider,
  Button,
} from 'antd';
import { PlusOutlined, DeleteOutlined } from '@ant-design/icons';
import moment from 'moment';
import XormContext from './utils';

const { TextArea } = Input;

const XormSection = props => {
  const { sectionKey, section } = props;
  const { _params, questions } = section;

  const renderQuestionType = question => {
    const {
      id, title, description, type, options, count, min, max,
      cols, rows, settings,
    } = question;

    switch (type) {
      case 'string':
        return <XStringInput key={id} section={sectionKey} id={id} type={type} title={title} description={description} />;
      
      case 'tidesc':
        return <XTiDesc key={id} type={type} title={title} />;

      case 'number':
        return <XNumberInput key={id} section={sectionKey} id={id} type={type} title={title} description={description} />;

      case 'text':
        return <XLargeInput key={id} section={sectionKey} id={id} type={type} title={title} description={description} />;

      case 'yes-no':
      case 'yes-no-dont':
      case 'single-choice':
        return <XSingleChoice key={id} section={sectionKey} id={id} type={type} title={title} description={description} options={options} />;

      case 'multiple-choice':
        return <XMultipleChoice key={id} section={sectionKey} id={id} type={type} title={title} description={description} options={options} />;

      case 'single-choice-select':
        return <XSelectChoice key={id} section={sectionKey} id={id} type={type} title={title} description={description} options={options} />;

      case 'date':
        return <XDateInput key={id} section={sectionKey} id={id} type={type} title={title} description={description} />;

      case 'time':
        return <XTimeInput key={id} section={sectionKey} id={id} type={type} title={title} description={description} />;

      case 'scale':
        return <XScaleInput key={id} section={sectionKey} id={id} type={type} title={title} description={description} min={min} max={max} />;

      case 'datatable':
        return <XDataTable key={id} section={sectionKey} id={id} type={type} title={title} description={description} rows={rows} cols={cols} settings={settings} />;

      case 'rate':
        return <XRateInput key={id} section={sectionKey} id={id} type={type} title={title} description={description} count={count} />;
    }
  };

  const renderCurrentQuestion = (currentQuestion) => {
    const {
      id, title, type,
    } = currentQuestion;

    return (
      <div className={`question ${type}`} key={id}>
        <div className="title">{ title }</div>
        <div className="value">
          { renderQuestionType(currentQuestion)}
        </div>
      </div>
    );
  };

  const renderQuestions = questions => {
    const currentQuestionsKeys = Object.keys(questions);

    return (
      <>
        { currentQuestionsKeys.map(questionKey => renderCurrentQuestion(questions[questionKey])) }
      </>
    );
  };

  return (
    <fieldset style={{ marginTop: '10em' }}>
      { questions ? renderQuestions(questions) : null }
    </fieldset>
  );
};

export default XormSection;

const XTiDesc = () => <></>;

const XStringInput = props => {
  const { section, id, description } = props;
  const { handleChange, getValue } = useContext(XormContext);
  return (
    <Form.Item label={description}>
      <Input type="text" value={getValue({ id, section, value: '' })} name={id} onChange={event => handleChange(event, { id, section })} />
    </Form.Item>
  );
};

const XLargeInput = props => {
  const { section, id, title, description } = props;
  const { handleChange, getValue } = useContext(XormContext);

  return (
    <Form.Item label={description}>
      <Input.TextArea rows={4} name={id} value={getValue({ id, section, value: '' })} onChange={event => handleChange(event, { id, section })} />
    </Form.Item>
  );
};

const XNumberInput = props => {
  const { section, id, title, description } = props;
  const { handleChange, getValue } = useContext(XormContext);

  return (
    <Form.Item label={description}>
      <InputNumber name={id} value={getValue({ id, section, value: '' })} onChange={event => handleChange(event, { id, section })} />
    </Form.Item>
  );
};

const XSingleChoice = props => {
  const {
    section, id, title, description, options,
  } = props;
  const { handleChange, getValue } = useContext(XormContext);

  return (
    <Form.Item label={description}  size="large">
      <Radio.Group 
        name={id}
        onChange={event => handleChange(event, { id, section })}
        value={getValue({ id, section, value: '' })}
      >
        { options.map(opt => <Radio key={opt["value"]} value={opt["value"]}>{opt["value"]}</Radio>) }
      </Radio.Group>
    </Form.Item>
  );
};

const XMultipleChoice = props => {
  const {
    section, id, title, description, options,
  } = props;
  const { handleChange, getValue } = useContext(XormContext);

  return (
    <Form.Item label={description}>
      <Checkbox.Group 
        name={id}
        onChange={event => handleChange(event, { id, section })}
        value={getValue({ id, section, value: [] })}
      >
        { options.map(opt => <Checkbox key={opt['value']} value={opt["value"]}>{opt["value"]}</Checkbox>) }
      </Checkbox.Group>
    </Form.Item>
  );
};

const XSelectChoice = props => {
  const {
    section, id, title, description, options,
  } = props;
  const { handleChange, getValue } = useContext(XormContext);

  return (
    <Form.Item name={id} label={description}>
      <Select onChange={event => handleChange(event, { id, section })} value={getValue({ id, section, value: '' })}>
        { Object.keys(options).map(key => <Select.Option key={key} value={key}>{options[key]}</Select.Option>) }
      </Select>
    </Form.Item>
  );
};

const XDateInput = props => {
  const {
    section, id, title, description, options,
  } = props;
  const { handleChange, getValue } = useContext(XormContext);

  return (
    <Form.Item name={id} label={description}>
      <DatePicker onChange={event => handleChange(event, { id, section })} value={getValue({ id, section, value: '' })} />
    </Form.Item>
  );
};

const XTimeInput = props => {
  const {
    section, id, title, description, options,
  } = props;
  const { handleChange, getValue } = useContext(XormContext);

  return (
    <Form.Item name={id} label={description}>
      <TimePicker
        onChange={event => handleChange(event, { id, section })}
        value={getValue({ id, section, value: '' })}
        defaultOpenValue={moment('00:00:00', 'HH:mm:ss')}
      />
    </Form.Item>
  );
};

const XScaleInput = props => {
  const {
    section, id, title, description, min, max,
  } = props;
  const { handleChange, getValue } = useContext(XormContext);

  return (
    <Form.Item label={description}>
      <Row justify="space-between">
        <Col>{min.text}</Col>
        <Col>{max.text}</Col>
      </Row>
      <Slider name={id} min={min.value} max={max.value} onChange={event => handleChange(event, { id, section })} 
        value={getValue({ id, section, value: '' })}
      />
    </Form.Item>
  );
};

const XRateInput = props => {
  const {
    section, id, title, description, count,
  } = props;
  const { handleChange, getValue } = useContext(XormContext);

  return (
    <Form.Item label={description}>
      <Rate name={id} count={count.value} onChange={event => handleChange(event, { id, section })}
        value={getValue({ id, section, value: '' })}
      />
    </Form.Item>
  );
};

const XDataTableInputText = props => {

  const {
    questionId, col, row, onChange, value
  } = props;
  
  const id = `${questionId}__row_${row}__col_${col}`;

  // return (<Input type="text" name={id} onChange={event => onChange(row, col, event.target.value)} />);
  return <TextArea value={value} rows={2} name={id} onChange={event => onChange(row, col, event.target.value)} />;
};

const XDataTableInputSingleChoice = props => {

  const {
    questionId, col, row, 
    choices, onChange
  } = props;
  
  const id = `${questionId}__row_${row}__col_${col}`;

  // return (<Input type="text" name={id} onChange={event => onChange(row, col, event.target.value)} />);
  return (
    <Form.Item size="large">
      <Radio.Group name={id} onChange={event => onChange(row, col, event.target.value)}>
        { choices.map(opt => <Radio key={opt.value} value={opt.value}>{opt.value}</Radio>) }
      </Radio.Group>
    </Form.Item>
  )
};

const XDataTable = props => {
  const {
    section, id, title, description, settings, cols, rows,
  } = props;

  const { handleChange, addValue, getValue } = useContext(XormContext);

  const initialValues = () => {
    const countRows = settings.rows.none ? settings.rows.displayedLinesDefault : rows.length;
    let newValues = [];

    for (let i=0; i < countRows; i++) {
      let obj = {};
      for (let j=0; j < cols.length; j++) {
        if (j == 0 && !settings.rows.none) {
          obj[`col_${j}`] = rows[i].text;
        } else {
          obj[`col_${j}`] = '';
        }
      }
      newValues.push({
        key: i,
        ...obj,
      });
    }

    return newValues;
  };

  const [values, setValues] = useState([]);
  useEffect(() => {
    setValues(getValue({ id, section, value: initialValues() }));
  }, []);

  useEffect(() => {
    handleChange(undefined, { id, section, value: values });
  }, [values]);

  const handleAddRow = () => {
    let obj = {};
    for (let j=0; j < cols.length; j++) {
      obj[`col_${j}`] = '';
    }

    setValues(currentValues => [
      ...currentValues,
      { key: currentValues.length, ...obj },
    ]);
  };

  const handleRemoveRow = index => {
    setValues(values.filter(v => v.key !== index));
  };

  const onInputChange = (row, col, value) => {
    const index = values.findIndex(v => v.key === row);
    const newItem = values[index];
    newItem[`col_${col}`] = value;

    setValues([
      ...values.slice(0, index),
      { ...newItem },
      ...values.slice(index + 1),
    ]);
  };

  const renderAddRowBtn = () => {
    if (settings.rows.none) {
      return <Button type="primary" size="small" onClick={() => handleAddRow()} icon={<PlusOutlined />}></Button>;
    }

    return null;
  };

  const renderTableRows = () => {
    const trLines = [];
    for (let xr = 0; xr < values.length; xr++) {
      trLines.push(
          <tr key={`row_${xr}`}>
            {
              cols.map((col, xc) => (
                xc == 0 && !settings.rows.none 
                ? <th key={`col_${xc}`} data-content={rows[xr].text}>{ rows[xr].text }</th>
                : <td key={`col_${xc}`} data-content={col.text}>
                  {col.type === "Single Choice" 
                  ? <XDataTableInputSingleChoice value={values[xr][`col_${xc}`]} section={section} questionId={id} row={xr} col={xc} choices={col.choices} onChange={onInputChange} />
                  : <XDataTableInputText value={values[xr][`col_${xc}`]} section={section} questionId={id} row={xr} col={xc} onChange={onInputChange} />
                  }
                </td>
              ))
            }
            {rows.length === 0 && <td>
              <Button type="primary" onClick={() => handleRemoveRow(xr)} shape="circle" size="small" icon={<DeleteOutlined />}></Button>
            </td>}
          </tr>
        // ))
      );
    }
    return trLines;
  };

  return (
    <Form.Item label={description}>
      <table className="datatable">
        <thead>
          <tr>
            {/* { settings && !settings.rows.exists ? null : <th /> } */}
            {/* <th> # </th> */}
            { cols.map((col, xc) => <th key={`col_h_${xc}`}>{col.text}</th>)}
            { rows.length === 0 && <td>#</td> }
          </tr>
        </thead>
        <tbody>
          { renderTableRows() }
        </tbody>
      </table>
      { renderAddRowBtn() }
      {/* <Button type="primary" onClick={(event) => handleAddRow()} icon={<PlusOutlined />}></Button> */}
    </Form.Item>
  );
};
