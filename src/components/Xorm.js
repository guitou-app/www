import React, {
  useState, useEffect, useMemo,
} from 'react';
import {
  useParams,
} from 'react-router-dom';
import {
  Form,
  Typography,
  Divider,
  Button,
  Spin,
  Result,
  Input,
  Checkbox,
  Row,
  Col,
  Radio,
  Popover,
  Select,
} from 'antd';
import { 
  CloseCircleOutlined, ArrowLeftOutlined,
  ArrowRightOutlined, QuestionCircleOutlined,
  CheckCircleOutlined, SaveOutlined,
} from '@ant-design/icons';
import { Liquid, Bullet } from '@ant-design/charts';
import { useQuery } from 'react-query';
import XormSection from './XormSection';
import { getXorm, getXormAvailableLanguages } from '../api/xorm.api';
import { saveData } from '../api/data.api';
import XormContext from './utils';
import { isArray } from '@craco/craco/lib/utils';

const { TextArea } = Input;
const { Option } = Select;


const isRowFilled = (value) => {
  return Object.keys(value).some(v => v !== 'key' && value[v] !== '');
};

const fetchXorm = async (id, language = '') => {
  const result = await getXorm(id, language);

  document.title = `Guitou - ${result.title}`;
  return result;
}

const Xorm = props => {
  const { id } = useParams();

  const [form] = Form.useForm();
  const [xorm, setXorm] = useState(null);
  const [save, setSave] = useState({ isSaving: false, isSuccessful: undefined });
  const [currentLanguage, setLanguage] = useState(null);
  const { isLoading, isLoadingLanguages, data: xormLanguages, refetch: refetchXormLanguage } = useQuery('fetch-xorm-languages',
    async () => {
      return await getXormAvailableLanguages(id);
    },
    {
      onSuccess: (languages) => {
        console.log('Lanaguages ', languages);
        if (languages.length > 0) {
          setLanguage(languages.find(l => l.default).language)
        } else {
          setLanguage('');
        }
      },
      enabled: false
    }
  );
  const [isLoadingXorm, setLoadingXorm] = useState(true);

  async function getXormLanguage(id, language = '') {
    const response = await fetchXorm(id, language);
    setXorm(response);
    setLoadingXorm(false);
  }

  useEffect(() => {
    if (!xormLanguages) {
      refetchXormLanguage();
    }
  }, []);

  useEffect(() => {
    getXormLanguage(id, currentLanguage);
  }, [currentLanguage]);

  // const { isLoading: isLoadingXorm, error, isError, data: xorm } = useQuery('fetch-xorm',
  //   async () => {
  //     return await getXorm(id);
  //   },
  //   {
  //     onSuccess: (result) => document.title = `Guitou - ${result.title}`,
  //   },
  // );
  const [data, setData] = useState({});
  const [questions, setQuestions] = useState([]);
  const [currentSectionIndex, setCurrentSectionIndex] = useState(0);

  const {
    coords,
  } = props;

  useEffect(() => {
    // console.log('xorm.... ', coords);
    if (!xorm) {
      return;
    }

    let allQuestions = [];

    Object.keys(xorm.xorm).forEach(sectionKey => {
      const section = xorm.xorm[sectionKey];
      const questions = Object.keys(section.questions).map(questionKey => ({ sectionKey, questionKey, answered: false }));
      allQuestions = [...allQuestions, ...questions];
    });
    setQuestions(allQuestions);
  }, [xorm]);

  const countSections = useMemo(() => (xorm !== null ? Object.keys(xorm.xorm).length : 0), [xorm]);
  const countQuestions = useMemo(() => questions.length, [questions]);
  const countAnsweredQuestions = useMemo(() => {
    let count = 0;

    Object.keys(data).forEach(sectionKey => {
      const section = data[sectionKey];
      Object.keys(section).forEach(questionKey => {
        if (
          xorm.xorm[sectionKey].questions[questionKey].type === 'datatable'
          && section[questionKey].some(v => isRowFilled(v))
        ) {
          count += 1;
        } else if (
          xorm.xorm[sectionKey].questions[questionKey].type === 'multiple-choice'
          && isArray(section[questionKey])
        ) {
          if (section[questionKey].length > 0) {
            count += 1;
          }
        } else if (typeof section[questionKey] === 'string' && section[questionKey] !== '') {
          count += 1;
        }
      });
    });

    return count;
  }, [data]);
  const liquidPercent = useMemo(() => countAnsweredQuestions / countQuestions, [countAnsweredQuestions, countQuestions]);

  const handleChange = (event, {
    id, section, value, ...rest
  }) => {
    let rValue;
    if (event && event.currentTarget) {
      event.persist();

      rValue = event ? event.currentTarget.value : value;
    } if (event && event.target) {
      rValue = event ? event.target.value : value;
    } else {
      rValue = event || value;
    }
    // console.log('handle change - addValue ', id, section, value);
    addValue({
      id,
      section,
      value: rValue,
      ...rest,
    });
  };

  const getValue = ({ id, section, value }) => {
    if (data.hasOwnProperty(section) && data[section].hasOwnProperty(id)) {
      return data[section][id];
    }
    // console.log('getValue - addValue ', id, section, value);
    return addValue({ id, section, value });
  };

  const addValue = ({
    id, section, value,
  }) => {
    // console.log('addValue ', id, section, value);
    if (id && section) {
      if (!data.hasOwnProperty(section)) {
        data[section] = {};
      }

      let rValue;
      if (xorm.xorm[section].questions[id]) {
        const { type, settings } = xorm.xorm[section].questions[id];

        if (type && type === 'number') {
          rValue = +value;
        } else {
          rValue = value;
        }
      } else {
        // The Question Input comes from a Table Question
        rValue = value;
      }

      // data[section] = {
      //   ...data[section],
      //   [id]: rValue,
      // };
      setData(data => ({
        ...data,
        [section]: {
          ...data[section],
          [id]: value,
        },
      }));

      return value;
    }

    throw new Error(`please add 'id & parent' field to the input: ${id} - ${section}`);
  };

  const handleSubmit = async () => {
    const dataToSave = {
      location: {
        coords: {
          accuracy: coords.accuracy,
          altitude: coords.altitude,
          altitudeAccuracy: coords.altitudeAccuracy,
          heading: coords.heading,
          latitude: coords.latitude,
          longitude: coords.longitude,
          speed: coords.speed,
        },
      },
      values: data,
    };
    // console.log('handleSubmit', dataToSave);

    setSave({ isSaving: true });
    const result = await saveData(id, dataToSave);
    // // console.log('result ', result);
    const { success } = result;
    setSave({ isSaving: false, isSuccessful: success });
  };

  const renderHeader = () => {
    const currentSectionKey = Object.keys(xorm.xorm)[currentSectionIndex];
    const currentSection = xorm.xorm[currentSectionKey];
    const { title } = currentSection._params;

    return (
      <div className="header">
        <div className="title">{ title }</div>
        <div className="step">
          <span>{ currentSectionIndex + 1 }</span>
          |
          <span>{ countSections }</span>
        </div>

        <Divider />
      </div>
    );
  };

  const renderQuestionType = (type, additionalData) => {
    const { options } = additionalData;

    if (type === "string") {
      return <Input size="large" placeholder="" />;
    }
    if (type === "multiple-choice") {
      return (
        <Checkbox.Group>
          { options.map(opt => <Checkbox 
              key={opt['value']}
              value={opt["value"]}
            >{opt["value"]}</Checkbox>)}
        </Checkbox.Group>
      );
    }
    if (type === "text") {
      return (
        <TextArea rows={6} />
      );
    }
    if (type === "yes-no") {
      return (
        <Radio.Group>
          { options.map(opt => <Radio 
              key={opt['value']}
              value={opt["value"]}
            >{opt["value"]}</Radio>)}
        </Radio.Group>
      );
    }
    return <div>{ type }</div>;
  };

  const renderCurrentSection = () => {
    const currentSectionKey = Object.keys(xorm.xorm)[currentSectionIndex];
    const currentQuestions = xorm.xorm[currentSectionKey].questions;
    const currentQuestionsKeys = Object.keys(currentQuestions);

    // return (
    //   <>
    //     { currentQuestionsKeys.map(questionKey => renderCurrentQuestion(currentQuestions[questionKey])) }
    //   </>
    // );
    return <XormSection section={xorm.xorm[currentSectionKey]} sectionKey={currentSectionKey} />;
  };

  const backPreviousQuestion = () => {
    if (currentSectionIndex > 0) {
      setCurrentSectionIndex(currentSectionIndex - 1);
      window.scrollTo(0, 0);
    }
  }

  const openNextQuestion = () => {
    // console.log('Open Next ', data);
    if (currentSectionIndex < Object.keys(xorm.xorm).length - 1) {
      setCurrentSectionIndex(currentSectionIndex + 1);
      window.scrollTo(0, 0);
    }
  }

  const renderFooter = () => {
    return (
      <div className="footer">
        <Button size={"large"} block onClick={backPreviousQuestion} disabled={currentSectionIndex === 0}>
          <ArrowLeftOutlined />
          Back
        </Button>
        {currentSectionIndex < Object.keys(xorm.xorm).length - 1 
          ? <Button type="primary" block size={"large"} onClick={() => openNextQuestion()} disabled={currentSectionIndex === Object.keys(xorm.xorm).length - 1}>
            Next
            <ArrowRightOutlined />
          </Button>
          : <Button htmlType="submit" type="primary" block onClick={() => handleSubmit()}>
            Send
            <SaveOutlined />
          </Button>
        }
      </div>
    )
  }

  const renderAvailableLanguages = () => {
    if (xormLanguages?.length <= 1) {
      return null;
    }

    return (
      <div className='name'>
        <span>Languages</span>
        <div>
          <Select
            style={{ width: 200 }}
            placeholder="Select a language"
            onChange={handleChangeLanguage}
            defaultValue={xormLanguages.find(l => l.default).language}
          >
            { xormLanguages.map(language => <Option value={language.language}>{language.language}</Option>)}
          </Select>
        </div>
      </div>
    );
  }

  const handleChangeLanguage = (value) => {
    console.log('handle change language ', value);
    setLanguage(value);
  }

  const configLiquid = {
    percent: liquidPercent,
    outline: {
      border: 4,
      distance: 8,
    },
    wave: {
      length: 128,
    },
    pattern: {
      type: 'line',
    },
    color: '#1890ff',
  };

  const configBullet = {
    data: [
      {
        title: '',
        ranges: [countQuestions / 3, 2 * (countQuestions / 3), countQuestions],
        measures: [countAnsweredQuestions],
        target: countQuestions,
      },
    ],
    measureField: 'measures',
    rangeField: 'ranges',
    targetField: 'target',
    xField: 'title',
    color: {
      range: ['#FFbcb8', '#FFe0b0', '#bfeec8'],
      measure: '#5B8FF9',
      target: '#39a3f4',
    },
    xAxis: {
      line: null,
    },
    yAxis: false,
    label: {
      target: true,
    },
  };

  const renderXorm = () => {
    if (!xorm || questions.length === 0) {
      return null;
    }

    return (
      <Spin
        style={{ position: 'fixed', zIndex: 1000, margin: 'auto', bottom: 0 }}
        size="large" spinning={save.isSaving || isLoadingXorm }
      >
        <div className="xorm">
          <XormContext.Provider value={{
            data, handleChange, addValue, getValue,
          }}
          >
            <article className="infos">
              <div>
                { renderAvailableLanguages() }
                <div className="name">
                  <span>Survey name:</span>
                  <Popover placement="topLeft" title={xorm.title}  arrowPointAtCenter trigger="click">
                    <div className="title">{xorm.title}</div>
                  </Popover>
                  <p>{ xorm.description }</p>
                </div>
                <div className="id">
                  <span>ID:</span>
                  {' '}
                  { xorm.id }
                </div>
                <div className="liquid">
                  <Liquid {...configLiquid} />
                </div>
                <div className="bullet">
                  <Bullet {...configBullet} />
                </div>
                <div className="stats">
                  <div className="questions">
                    <div><QuestionCircleOutlined style={{ color: "#1890ff", fontSize: '18px' }} /></div>
                    <span>Questions</span>
                    <span className="accent">{ countQuestions }</span>
                  </div>
                  <div className="responses">
                    <div><CheckCircleOutlined style={{ color: '#1890ff', fontSize: '18px' }} /></div>
                    <span>Responses</span>
                    <span className="accent">{ countAnsweredQuestions }</span>
                  </div>
                </div>
              </div>
            </article>
            <section className="survey">
              <Form
                form={form}
                layout="vertical"
              >
                { renderHeader() }
                { renderCurrentSection() }
                { renderFooter() }
              </Form>
            </section>
          </XormContext.Provider>
        </div>
      </Spin>
    );
  };

  const renderMain = () => {
    return (
      <main className="wrapper">
        {
          save.isSuccessful
            ? (
              <Result
                status="success"
                title="Thank you!!"
                subTitle="You have successfully fill the questionnaire. Thank you very much."
              />
            )
            : renderXorm()
        }
      </main>
    );
  };

  return renderMain();
};

export default Xorm;
