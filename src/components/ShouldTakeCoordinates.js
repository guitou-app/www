import { Spin } from 'antd';
import React, { useEffect } from 'react';
import { useQuery } from 'react-query';
import { useParams } from 'react-router-dom';
import { shouldTakeGeolocationPosition } from '../api/xorm.api';
import GettingCoordinates from './GettingCoordinates';
import Xorm from './Xorm';

const ShouldTakeCoordinates = () => {
  const { id } = useParams();

  const { isLoading, isError,
    data: shouldTakeGeoPosition, refetch: refetchTakeGeoPosition
  } = useQuery('fetch-take-geo-position',
    async () => {
      return await shouldTakeGeolocationPosition(id);
    },
    {
      enable: false
    },
  );

  useEffect(() => {
    refetchTakeGeoPosition();
  }, [id]);

  return (
    <main style={{ display: 'flex', height: '100vh', alignItems: 'center', justifyContent: 'center' }}>
      <Spin size="large" spinning={isLoading}>
        {
          shouldTakeGeoPosition
            ? <GettingCoordinates />
            : <Xorm />
        }
      </Spin>
    </main>
  );
}

export default ShouldTakeCoordinates;
