import { createContext } from 'react';

const XormContext = createContext({
  data: {},
});

export default XormContext;
