const { REACT_APP_API_URL } = process.env;

const XORM_URL = id => `${REACT_APP_API_URL}/api/projects/x/${id}`;

const getXorm = (id, language = '')=> fetch(`${XORM_URL(id)}${language ? `?language=${language}` : ''}`)
  .then(response => response.json());

const getXormAvailableLanguages = id => fetch(`${XORM_URL(id)}/translations`)
  .then(response => response.json());

const shouldTakeGeolocationPosition = id => fetch(`${XORM_URL(id)}/takeGeoPosition`)
  .then(response => response.json());

export {
  getXorm,
  getXormAvailableLanguages,
  shouldTakeGeolocationPosition,
};
