export const saveData = (id, data) => fetch(`${process.env.REACT_APP_API_URL}/api/data/xorm/${id}`, {
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  method: 'POST',
  body: JSON.stringify(data),
}).then(response => response.json());
