import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import { QueryClient, QueryClientProvider } from 'react-query';
import 'antd/dist/antd.css';
import './App.less';
import Home from './components/Home';
import Header from './components/Header';
import ShouldTakeCoordinates from './components/ShouldTakeCoordinates';

const queryClient = new QueryClient();

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <Router>
        <div>
          <Header />

          {/* A <Switch> looks through its children <Route>s and
              renders the first one that matches the current URL. */}
          <Switch>
            <Route path="/" exact>
              <Home />
            </Route>
            <Route path="/about">
              <div><h1>About</h1></div>
            </Route>
            <Route path="/x/:id">
              {/* <GeolocatedXorm onSuccess={position => console.log("onSuccess ", position)} /> */}
              {/* <Xorm /> */}
              {/* <GettingCoordinates /> */}
              <ShouldTakeCoordinates />
            </Route>
          </Switch>
        </div>
      </Router>
    </QueryClientProvider>
  );
}

export default App;
